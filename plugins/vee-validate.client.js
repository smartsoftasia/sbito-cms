import Vue from 'vue'
import {
  ValidationProvider,
  ValidationObserver,
  setInteractionMode,
  extend,
} from 'vee-validate'
import {
  required,
  email,
  min,
  max,
  confirmed,
  regex,
  min_value,
  length,
} from 'vee-validate/dist/rules'

setInteractionMode('eager')

// Add a rule.
extend('secret', {
  validate: (value) => value === 'example',
  message: 'This is not the magic word',
})

// Add the required rule
extend('required', {
  ...required,
  message: (fieldName) => `กรุณากรอก ${fieldName}`,
})

// Add Phone rule
extend('regex', {
  ...regex,
  message: 'ผิดรูปแบบ',
})

// Add the email rule
extend('email', {
  ...email,
  message: () => `กรุณากรอกอีเมลให้ถูกต้อง`,
})

extend('length', length)

// Add the min rule
extend('min', {
  ...min,
  params: ['min'],
  message: (fieldName, { min }) => `กรุณากรอก ${fieldName} อย่างน้อย ${min}`,
})

extend('min_value', {
  ...min_value,
  params: ['min'],
  message: (fieldName, { min }) => `${fieldName} ต้องเป็นจำนวนอย่างน้อย ${min}`,
})

extend('positiveNumber', {
  validate: (value) => !isNaN(value) && value > 0,
  message: (fieldName) => `${fieldName} ต้องมากกว่า 0`,
})

// Add the max rule
extend('max', {
  ...max,
  params: ['max'],
  message: (fieldName, { max }) => `${fieldName} จำกัด ${max} ตัวอักษร`,
})

const dateFormat = 'YYYY-MM-DD'

// Add minDate rule
extend('minDate', {
  params: ['limit', 'included'],
  validate: (value, { limit, included = true }) => {
    if (!limit) return true

    limit = moment(limit, dateFormat)
    value = moment(value, dateFormat)
    return included ? value.isSameOrAfter(limit) : value.isAfter(limit)
  },
  message: (fieldName, placeholders) => {
    const dateTime = moment(placeholders.limit).locale('th')
    const year = parseInt(dateTime.format('YYYY')) + 543
    return `${fieldName} ต้องเลือกวันเดียวกันหรือหลังจากวันที่ ${dateTime.format(
      `D MMMM ${year}`
    )}`
  },
})

extend('maxDate', {
  params: ['limit', 'included'],
  validate: (value, { limit, included = true }) => {
    if (!limit) return true

    limit = moment(limit, dateFormat)
    value = moment(value, dateFormat)
    return included ? value.isSameOrBefore(limit) : value.isBefore(limit)
  },
  message: (fieldName, placeholders) => {
    const dateTime = moment(placeholders.limit).locale('th')
    const year = parseInt(dateTime.format('YYYY')) + 543
    return `${fieldName} ต้องเลือกวันเดียวกันหรือก่อนวันที่ ${dateTime.format(
      `D MMMM ${year}`
    )}`
  },
})

extend('confirmed', confirmed)

// Register it globally
Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
