import Vue from 'vue'
import vuedraggable from 'vuedraggable'

Vue.component('draggable', vuedraggable)
