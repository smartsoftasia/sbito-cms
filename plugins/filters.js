import Vue from 'vue'

Vue.filter('thDatetimeFormat', (data) =>
  moment(data).format('dddd D MMMM YYYY, hh:mm:ss')
)

Vue.filter('thDateFormat', (data) => moment(data).format('dddd D MMMM YYYY'))

const filters = {
  currencyFormat(value) {
    return value && !isNaN(value) && value !== null
      ? parseFloat(value || 0).toLocaleString(undefined, {
          minimumFractionDigits: 2,
        })
      : '-'
  },

  dateFormat(value) {
    const dateTime = moment(value)
    return dateTime.format(`D MMMM YYYY`)
  },

  dateUpdatedFormat(value) {
    const dateTime = moment(value)
    return dateTime.format(`D MMMM YYYY HH:mm:ss`)
  },

  dateCETH(value) {
    const dateTime = moment(value).locale('th')
    const year = parseInt(dateTime.format('YYYY')) + 543
    return dateTime.format(`D MMMM ${year}`)
  },
  dateCETHNumber(value) {
    return new Date(value).toLocaleDateString('th-TH', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
    })
  },

  dateTimeCETH(value) {
    const dateTime = moment(value).locale('th')
    const year = parseInt(dateTime.format('YYYY')) + 543
    return dateTime.format(`D MMMM ${year} hh:mm น.`)
  },
  numberFormat(value) {
    return value !== null ? parseInt(value || 0).toLocaleString() : '-'
  },
  TID(value) {
    return value ? value.padStart(6, '0') : '-'
  },
}

_.each(filters, (value, key) => {
  Vue.filter(key, value)
})
