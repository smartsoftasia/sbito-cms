/* eslint-disable prettier/prettier */
import Vue from 'vue'
import {
  DatePicker
} from 'ant-design-vue'
// import 'ant-design-vue/dist/antd.css'
import 'ant-design-vue/lib/date-picker/style/index.css'

Vue.config.productionTip = false

Vue.component(DatePicker.name, DatePicker)
