export default {
  data() {
    return {
      startValue: null,
      endValue: null,
    }
  },
  watch: {
    startValue(val) {
      // console.log('startValue', val)
    },
    endValue(val) {
      // console.log('endValue', val)
    },
  },
  methods: {
    disabledStartDate(startValue) {
      const endValue = this.endValue
      if (!startValue || !endValue) {
        return false
      }
      return startValue.valueOf() > endValue.valueOf()
    },
    disabledEndDate(endValue) {
      const startValue = this.startValue
      if (!endValue || !startValue) {
        return false
      }
      return startValue.valueOf() >= endValue.valueOf()
    },
  },
}
