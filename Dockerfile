FROM node:12-stretch

WORKDIR /usr/src/app

COPY .npmrc package.json package-lock.json ./

RUN npm install

COPY . /usr/src/app

RUN npm run build

CMD ["npm", "start"]

EXPOSE 3000
