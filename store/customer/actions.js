export default {
  fetchCustomerList({}, params) {
    return this.$axios.$get('/api/customer/cms/get_list', { params })
  },
  fetchCustomerDetail({}, params) {
    return this.$axios.$get('/api/customer/cms/get_detail', { params })
  },
  fetchCustomerPointHistory({}, params) {
    return this.$axios.$get('api/customer/cms/point_history', { params })
  },
  updateNotiBlockStatus({}, payload) {
    return this.$axios.$post('api/customer/cms/update_block_noti', payload)
  },
}
