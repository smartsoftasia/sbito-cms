export default {
  upload({}, payload) {
    let getFormData = (obj) => {
      const formData = new FormData()
      Object.keys(obj).forEach((key) => {
        if (key == 'file' && obj[key] && obj[key].name) {
          formData.append(key, obj[key], obj[key].name)
        } else {
          formData.append(key, obj[key])
        }
      })
      return formData
    }
    return this.$axios
      .post('api/file/upload', getFormData(payload.data), {
        ...payload.config,
      })
      .then((res) => Promise.resolve(res.data))
  },
}
