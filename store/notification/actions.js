export default {
  fetchNotification({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('/api/notification/cms/get_detail', {
        params: payload,
      })
      resolve(res.data)
    })
  },
  fetchNotificationList({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('/api/notification/cms/get_list', {
        params: payload,
      })
      resolve(res.data)
    })
  },
  fetchCustomerTarget({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get(
        '/api/notification/cms/count_customer_target',
        {
          params: payload,
        }
      )
      resolve(res.data)
    })
  },
  createNotification({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/notification/cms/create', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  updateNotification({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/notification/cms/update', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  deleteNotification({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/notification/cms/delete', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
}
