export default {
  fetchPointList({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('/api/point/cms/get_list', {
          params: payload,
        })
        .then((res) => resolve(res.data))
    })
  },

  exportPoint({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('/api/point/cms/export', {
          params: payload,
        })
        .then((res) => resolve(res.data))
    })
  },

  checkImportPoint({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('/api/point/cms/check_import', {
          params: payload,
        })
        .then((res) => resolve(res.data))
    })
  },

  importPoint({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/point/cms/import', payload).then((res) => {
        resolve(res.data)
      })
    })
  },

  adjustPoint({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/point/cms/adjust_point', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
}
