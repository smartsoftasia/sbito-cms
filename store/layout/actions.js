export default {
  setBreadcrumb({ commit }, payload) {
    commit('SET_BREADCRUMB', payload)
  },
}
