export default {
  fetchReward({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('/api/reward/cms/get_detail', {
        params: payload,
      })
      resolve(res.data)
    })
  },
  searchReward({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('/api/reward/cms/search', {
        params: payload,
      })
      resolve(res.data)
    })
  },
  fetchRewardList({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('/api/reward/cms/get_list', {
        params: payload,
      })
      resolve(res.data)
    })
  },
  fetchSummary({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('/api/reward/cms/get_summary', {
        params: payload,
      })
      resolve(res.data)
    })
  },
  createReward({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/reward/cms/create', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  updateReward({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/reward/cms/update', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  deleteReward({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/reward/cms/delete', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  sortRewardList({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/reward/cms/sort', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  // eslint-disable-next-line prettier/prettier
  fetchRewardType({
    state,
    commit
  }) {
    if (state.rewardType.length) return
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('/api/reward/data/get_type_list')
      if (res.data.result) {
        commit('SET_REWARD_TYPE', res.data.result)
        resolve()
      }
    })
  },
  fetchTransactionList({}, payload) {
    return new Promise(async (resolve, reject) => {
      this.$axios
        .get('/api/reward/cms/get_transaction_list', {
          params: payload,
        })
        .then((res) => resolve(res.data))
    })
  },
  exportTransaction({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('/api/reward/cms/export_transaction', {
          params: payload,
        })
        .then((res) => resolve(res))
    })
  },
}
