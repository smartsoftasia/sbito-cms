export default {
  rewardOption: (state) =>
    state.rewardType.map((o) => ({
      value: o.id,
      text: o.reward_type_name,
    })),
}
