export default {
  fetchVersion({}, payload) {
    return new Promise((resolve) => {
      this.$axios
        .get('api/application_version/cms/get_detail', {
          params: payload,
        })
        .then((res) => resolve(res.data))
    })
  },
  updateForceVersion({}, payload) {
    return new Promise((resolve) => {
      this.$axios
        .post('api/application_version/cms/update', payload)
        .then((res) => {
          resolve(res.data)
        })
    })
  },

  fetchFreezeType({}) {
    return new Promise((resolve) => {
      this.$axios
        .get('api/application_version/cms/freeze_type')
        .then((res) => resolve(res.data))
    })
  },

  updateFreezeApp({}, payload) {
    return new Promise((resolve) => {
      this.$axios
        .post('api/application_version/cms/freeze', payload)
        .then((res) => {
          resolve(res.data)
        })
    })
  },
}
