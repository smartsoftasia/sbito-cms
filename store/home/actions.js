export default {
  fetchBannerList({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('/api/banner/cms/get_list', {
          params: payload,
        })
        .then((res) => resolve(res.data))
    })
  },

  fetchBannerDetail({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('/api/banner/cms/get_detail', {
          params: payload,
        })
        .then((res) => resolve(res.data))
    })
  },

  createBanner({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('api/banner/cms/create', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  updateBanner({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('api/banner/cms/update', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  deleteBanner({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('api/banner/cms/delete', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  sortBannerList({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/banner/cms/sort', payload).then((res) => {
        resolve(res.data)
      })
    })
  },

  fetchPromotion({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('/api/promotion/cms/get_detail', {
        params: payload,
      })
      resolve(res.data)
    })
  },
  fetchPromotionList({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('/api/promotion/cms/get_list', {
        params: payload,
      })
      resolve(res.data)
    })
  },
  searchPromotion({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('/api/promotion/cms/search', {
        params: payload,
      })
      resolve(res.data)
    })
  },
  createPromotion({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/promotion/cms/create', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  updatePromotion({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/promotion/cms/update', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  deletePromotion({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/promotion/cms/delete', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  sortPromotionList({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/promotion/cms/sort', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  fetchPopupDetail() {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('/api/popup/cms/get_detail')
        .then((res) => resolve(res.data))
    })
  },
  updatePopup({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/popup/cms/update', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
  deletePopup({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/popup/cms/delete', payload).then((res) => {
        resolve(res.data)
      })
    })
  },

  fetchInviteFriend({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('api/invite_friend/cms/get_detail')
      resolve(res.data)
    })
  },
  updateInviteFriend({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('api/invite_friend/cms/update', payload).then((res) => {
        resolve(res.data)
      })
    })
  },

  fetchRewardBanner({}, payload) {
    return new Promise(async (resolve, reject) => {
      const res = await this.$axios.get('api/reward_banner/cms/get_detail')
      resolve(res.data)
    })
  },
  updateRewardBanner({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('api/reward_banner/cms/update', payload).then((res) => {
        resolve(res.data)
      })
    })
  },

  fetchCampaignReward({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('/api/campaign_reward/cms/get_list', {
          params: payload,
        })
        .then((res) => resolve(res.data))
    })
  },
  deleteCampaignReward({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .post('api/campaign_reward/cms/delete', payload)
        .then((res) => {
          resolve(res.data)
        })
    })
  },
  fetchCampaignRewardDetail({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('/api/campaign_reward/cms/get_detail', {
          params: payload,
        })
        .then((res) => resolve(res.data))
    })
  },

  createCampaignReward({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .post('api/campaign_reward/cms/create', payload)
        .then((res) => {
          resolve(res.data)
        })
    })
  },
  updateCampaignReward({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .post('api/campaign_reward/cms/update', payload)
        .then((res) => {
          resolve(res.data)
        })
    })
  },
  sortCampaignReward({}, payload) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/api/campaign_reward/cms/sort', payload).then((res) => {
        resolve(res.data)
      })
    })
  },
}
