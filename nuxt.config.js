const pkg = require('./package')
const webpack = require('webpack')
require('dotenv').config()

module.exports = {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: 'SBITO Mobile Application',
    meta: [
      {
        charset: 'utf-8',
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description,
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
      },
    ],
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff',
  },

  /*
   ** Global CSS
   */
  css: [
    '@/assets/scss/bootstrap/custom.scss',
    '@fortawesome/fontawesome-svg-core/styles.css',
  ],

  styleResources: {
    scss: ['./assets/scss/_import/_variables.scss'],
  },

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    // { src: '~/plugins/axios', ssr: false },
    {
      src: '~/plugins/vee-validate.client',
      ssr: false,
    },
    {
      src: '~/plugins/font-awesome',
      ssr: true,
    },
    {
      src: '~/plugins/vue-cleave',
      ssr: false,
    },
    {
      src: '~/plugins/vue-scrollto',
      ssr: false,
    },
    {
      src: '~/plugins/moment-locale.js',
      ssr: true,
    },
    {
      src: '~/plugins/vuedraggable.client.js',
      ssr: false,
    },
    {
      src: '~/plugins/antdv.js',
      ssr: true,
    },
    {
      src: '~/plugins/v-calendar.js',
      ssr: false,
    },
    {
      src: '~/plugins/filters.js',
      ssr: true,
    },
    {
      src: '~/plugins/v-click-outside',
      ssr: false,
    },
  ],

  buildModules: [
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',

    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',

    '@nuxtjs/style-resources',
    '@nuxtjs/global-components',
  ],

  /**
   * Dotenv module configuration
   */
  dotenv: {
    /* module options */
  },

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.BASE_URL,
    credentials: true,
  },

  auth: {
    redirect: {
      login: '/cms/auth/login',
      logout: '/cms/auth/login',
      home: '/cms',
    },

    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api/admin/login',
            method: 'post',
            propertyName: 'result.token',
          },
          user: {
            url: '/api/admin/myprofile',
            method: 'get',
            propertyName: 'result.member',
          },
        },
        tokenType: false,
      },
    },
  },

  router: {
    middleware: ['auth'],
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */

    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },

    plugins: [
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new webpack.ProvidePlugin({
        _: 'lodash',
        moment: 'moment',
      }),
    ],
  },

  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0', // default: localhost
  },
}
